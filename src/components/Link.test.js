import React from 'react';
import renderer from 'react-test-renderer';
import Link from './Link';

test('Link changes the class when hovered', () => {
    const component = renderer.create(
        <Link page="https://www.kominfo.go.id">Kominfo</Link>,
    );
    let tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    // trigger callback secara manual
    tree.props.onMouseEnter();
    // re-rendering
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();

    // trigger callback secara manual
    tree.props.onMouseLeave();
    // re-rendering
    tree = component.toJSON();
    expect(tree).toMatchSnapshot();
});
